#!/bin/bash

#
#
# SCRIPT NO EJECUTABLE
#
#
#



exec >> {{ log_file.log }}
REMOTE_TMP="{{ remote_dir }}"
SERVER_DIR="{{ server_dir }}"
WORLDS=( world world_nether world_the_end {{ ...and more }} )

echo [`date "+%D %X"`] Si el server está levantado, paramos la instancia para realizar el backup
if [[ `systemctl status mc-server.service | grep -ic running` -eq 1 ]]; then
	sudo su - minecraft -c 'screen -r -S minecraft -X eval "stuff \"broadcast &cEl servidor va a reiniciarse para hacer el backup en 30 segundos\"\015"'
	sleep 30
	sudo su - minecraft -c 'screen -r -S minecraft -X eval "stuff \"stop\"\015"'
fi

echo [`date "+%D %X"`] Realizamos backup comprobando que el servidor remoto esté up
for i in `seq 0 3`; do
	UP=`ping -c 1 {{ remote_ip }} 2>/dev/null | grep -c "1 received"`
	[[ $UP -eq 1 ]] && break
done

if [[ $UP -eq 1 ]]; then
	echo [`date "+%D %X"`] Copiamos...
	for i in "${WORLDS[@]}"; do
		echo [`date "+%D %X"`] ...mundo $i
		rsync -avzd $SERVER_DIR/$i 192.168.1.101:${REMOTE_TMP}
	done
	
	echo [`date "+%D %X"`] Comprobamos antes si ya hay uno del dia hecho
	if [[ $(ssh raspi ls {{ remote_bk_dir }} | grep -c `date +%F`) -eq 0 ]]; then
		echo [`date "+%D %X"`] Empaquetamos en remoto los ficheros creados ahi
		ssh raspi 'cd Public/tmp && tar czvf /home/pi/Public/worlds_bk/worlds_`date +%F`.tar.gz *'
	else
		echo [`date "+%D %X"`] Omitimos.
	fi

	#echo Eliminamos los ficheros temporales
	#ssh raspi rm -rf /home/pi/Public/tmp/*
	echo [`date "+%D %X"`] Backup hecho!!
else
	echo "[`date "+%D %X"`] No se pudo conectar con el servidor remoto. Guardamos en local"
	cd $SERVER_DIR && tar czvf {{ nombre_fichero_bk}}_`date +%F`.tar.gz $WORLDS[@]
fi

echo [`date "+%D %X"`] Levantamos instancia
sudo systemctl start mc-server.service

echo [`date "+%D %X"`] Finalizado 
echo "-----------------------"
