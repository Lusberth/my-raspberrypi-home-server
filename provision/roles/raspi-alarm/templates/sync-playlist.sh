#!/bin/bash
# Author: Ander
# Sincronizar playlist

# Comprobar que el vantergos está up para sincronizar

UP=`ping -c 1 vantergos 2>/dev/null | grep -c "1 received"`

if [[ $UP -eq 1 ]]; then
	rsync -av --update --delete vantergos:/home/ander/randlist/pool_lst/ Music/
else
	echo "No se pudo sincronizar, estaba down"
	exit 1
fi

