#!/bin/bash
#
# Author: Ander
#

. /home/pi/bin/include/funcs


POOL=$HOME/Music
 
echo Checking if mplayer exists... `checking mplayer`
# If mplayer isn't installed, exit!
[[ `checking mplayer | grep -vc "OK"` -eq 1 ]] && exit 2

# Abort in case of mplayer is running
[[ `pgrep mplayer | wc -l` -gt 0 ]] && \
echo "You are playing other stuff with mplayer. Aborting" && exit 1

setdir

# start of loop
while [[ COUNT -lt 14 ]]; do
   # Permit live modifications in the $POOL folder
   RAND_SONG=`ls $POOL | egrep ".ogg$|.mp3$" | sort --random-sort | head -1`

   # Avoid play one song twice
   if [[ "$CURRENT_SONG" != "$RAND_SONG" ]]; then
	let COUNT=1+COUNT
	mplayer $POOL/"$RAND_SONG"
   else
     continue
   fi

   # Break the loop when the user hits CTRL + C
   [[ `echo $?` -eq 1 ]] && break

   CURRENT_SONG=$RAND_SONG
 done
