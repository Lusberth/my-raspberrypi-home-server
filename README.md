**Aviso a navegantes**: Este es un proyecto siempre en construcción y hay datos incompletos, pendientes de documentar o ampliables. Debido a que no uso Windows, la administración remota de la Raspberry Pi se realiza con un GNU/Linux, que es mi portátil, y por tanto, toda intervención externa a la Raspberry serán de acciones a realizar usando GNU/Linux que se llamará de ahora en adelante _Host_

**Leyenda**: Todo texto contenido en doble corchete `{{ }}` son variables que has de sustituir según corresponda.

# Puesta a punto

Se graba la ISO de Raspbian stretch en tarjeta micro SD. Elijo la versión lite, sin escritorio, ya que se pretende que sea un servidor core, sin GUI. Puedes elegir [tu imagen aquí](https://www.raspberrypi.org/downloads/).

Introducimos la microSD en un lector de tarjetas en nuestro Host (quizás sea necesario un adaptador SD-MicroSD). Nos aseguramos que estamos apuntando al disco correcto haciendo un `lsblk`. La microSD en cuestión debería ser nombrado como `/dev/mmcblk0`, así que apuntad bien a dónde grabáis.

```bash
sudo -i
dd if={{ /ruta/hacia/imagen de raspbian }} of={{ microSD }}
```
_Bonus_: [Instalación en disco encriptado](https://carlo-hamalainen.net/2017/03/12/raspbian-with-full-disk-encryption/)

Una vez realizado, configurar wifi y acceso SSH. La idea es no tener que enchufar teclado ni pantalla. Tras grabar la imagen, seguimos en nuestro Host para continuar configurando los accesos. Veremos ahora dos particiones y montamos la /boot de la microSD en un punto de montaje (mountpoint) sea con `mount` o desde nuestro gestor de archivos gráfico (Thunar, Nautilus...).

A continuación, creamos el archivo wpa_supplicant.conf en carpeta {{ mountpoint }}/boot con el siguiente contenido:

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=ES

network={
        ssid={{ Tu wifi }}
        psk={{ La clave de tu wifi }}
        key_mgmt=WPA-PSK
}
```

Para activar el SSH es tan sencillo como hacer: 

`touch {{ mountpoint }}/boot/ssh`

No hay que redimensionar la partición, ya que se realiza de manera automática en el primer arranque

## Securización

Antes de iniciar cualquier proyecto, es necesario poner un mínimo de seguridad. Por defecto, tenemos el usuario pi con una contraseña por defecto que se puede encontrar en la documentación oficial públicamente, y tiene permisos de administrador. 

Aquí tenemos dos opciones: 

1. Cambiar la pass de pi
2. Bloquear usuario y crear otro como sudoer

### Configurar el acceso por clave pública

1. Creamos primero nuestro par de claves en el Host

```bash
ssh-keygen -t rsa -b 2048

# Nos preguntará cómo querremos que se llame el fichero e introduciremos un nombre distinto:
Enter file in which to save the key (/home/[user]/.ssh/id_rsa): /home/{{ tu_usuario_Host }}/.ssh/raspi

```

No añadiremos contraseña a la clave de momento, así que finalizamos pulsando Enter varias veces.

2. Una vez terminado, copiamos la clave tal que:

```bash
ssh-copy-id -i .ssh/raspi.pub {{ usuario_raspi }}@{{ IP o hostname raspberrypi }} 

```
3. Para facilitar el acceso, creamos el fichero ~/.ssh/config con lo siguiente:


```
Host {{ IP o hostname raspi }}
	user pi
	identitiesonly yes
	identityfile ~/.ssh/raspi
```
4. En nuestra Raspberrypi configuraremos modificaremos las opciones del /etc/ssh/sshd_config tal que queden así. No es necesario borrar líneas del fichero. Es importante, para aceptar la codificación de caracteres adecuada añadir la entrada AcceptEnv:

```

Protocol 2

LoginGraceTime 1m
PermitRootLogin  prohibit-password
StrictModes yes
MaxAuthTries 3
MaxSessions 3

PubkeyAuthentication yes

AuthenticationMethods publickey

AcceptEnv LANG LC_*

PermitEmptyPasswords no
UsePAM no

```
En el lado del cliente (desde nuestro PC), podemos enviarle la codificación introduciendo la línea `SendEnv LANG LC_*` en el fichero /etc/ssh/ssh_config si queremos una configuración global o en ~/.ssh/config para un usuario y host específico.


5. Finalmente, reiniciamos con un `sudo systemctl reload ssh`

_Más información_: [Securing your Raspberri Pi](https://www.raspberrypi.org/documentation/configuration/security.md)

## Mantenimiento

Backup del disco entero
`ssh raspi "sudo dd if=/dev/mmcblk0 | gzip -1 - " | dd of=raspbian-v002.img.gz`

En la carpeta de provisión está el playbook de ansible en desarrollo donde se escribirá todo lo necesario para provisionar los proyectos a continuación. Solo está por amor al arte, así que tampoco estará completa y seguramente tenga errores.

Error de actualización de repositorio:

```
E: The repository 'https://dev2day.de/pms jessie Release' does no longer have a Release file.

```
Al parecer, se trataba de una repo del Plex Media Server. Pues al no utilizarlo, comento la línea y se soluciona el error.


# Proyectos

## Alarma con playlist 

Lo primero que vamos a necesitar son unos altavoces USB y un cargador para enchufar los altavoces, ya que si los conectamos a los puertos USD de la raspberrypi generan ruido eléctrico que se escucha en los altavoces como un pitido. 

1. Instalamos el paquete mplayer mediante `sudo apt install mplayer`


2. Establecemos una _pool_, que es una carpeta que va a contener las canciones.
3. Creamos el siguiente script:

```bash
#!/bin/bash
#
# Author: Lus
#

POOL={{ /ruta/de/tu/pool }}
 
# Abort in case of mplayer is running
[[ `pgrep mplayer | wc -l` -gt 0 ]] && \
echo "You are playing other stuff with mplayer. Aborting" && exit 1

# start of loop
while [[ COUNT -lt 14 ]]; do
   # Permit live modifications in the $POOL folder
   RAND_SONG=`ls $POOL | egrep ".ogg$|.mp3$" | sort --random-sort | head -1`

   # Avoid play one song twice
   if [[ "$CURRENT_SONG" != "$RAND_SONG" ]]; then
	let COUNT=1+COUNT
	mplayer $POOL/"$RAND_SONG"
   else
     continue
   fi

   # Break the loop when the user hits CTRL + C
   [[ `echo $?` -eq 1 ]] && break

   CURRENT_SONG=$RAND_SONG
 done

```

4. Le damos permisos de ejecución con `chmod 755 {{ tu_script }}`
5. Configuramos el crontab tal que:

```
### Alarmas para días laborales
02 7 * * 1-5 {{ /ruta/hacia/tu_script }} 
```
6. Para parar la alarma, simplemente enviaremos el comando `pkill mplayer` a nuestra raspberri pi.

#### Bonus

Podemos utilizar rsync para sincronizar el pool de canciones desde el host hasta la raspi. Para ello, en nuestro PC Linux configuramos el siguiente script que lo meteremos en el crontab:

```bash
#!/bin/bash
# Author: Lus
# Sincronizar playlist

# Comprobar que la raspi está up para sincronizar con el host que contiene las canciones
UP=`ping -c 1 {{ host }} 2>/dev/null | grep -c "1 received"`

if [[ $UP -eq 1 ]]; then
	rsync -av --update --delete {{ /pool/del/host }} {{ IP o hostname de la raspberry pi }}:{{ /pool/de/raspberry }}
else 
	echo "No se pudo sincronizar, estaba down"
	exit 1
fi
```
Finalmente, nos aseguramos de que el cron esté activo con un `service cron status`. 

#### Posibles problemas y alternativa

Por problema de codecs en ARM (intuyo que es por la arquitectura), algunas canciones no se reproducen correctamente con el mplayer:

```
MPlayer 1.3.0 (Debian), built with gcc-6.2.1 (C) 2000-2016 MPlayer Team
do_connect: could not connect to socket

[...]

==========================================================================
Opening audio decoder: [ffmpeg] FFmpeg/libavcodec audio decoders
libavcodec version 57.64.101 (external)
[vorbis @ 0x75eb8978]Extradata missing.
Could not open codec.
ADecoder init failed :(
ADecoder init failed :(
Requested audio codec family [vorbis] (afm=libvorbis) not available.
Enable it at compilation.
Opening audio decoder: [tremor] Ogg/Vorbis audio decoder
ad_vorbis, extradata seems to be absent!, exit
ADecoder init failed :(
ADecoder init failed :(
Cannot find codec for audio format 0x566F.
Audio: no sound
Video: no video


Exiting... (End of file)
```
La solución alternativa es el uso de VLC en línea de comandos. Por tanto, solo necesitaremos intalar el VLC y en el script anterior de alarma, sustituimos `mplayer` por `cvlc --play-and-exit`. Así haremos que se puedan reproducir todas las canciones sin problemas.

Hay que tener en cuenta que esta solución requiere de un script adicional para parar la alarma que consta de los siguientes comandos en una sola línea `pkill {{ script de alarma.sh }} && pkill vlc` (quien dice script también dice un alias en el bash). 

## Kanboard

Se crea el playbook kanboard-play.yml (ver carpeta provisión) con las instrucciones de la documentación: https://docs.kanboard.org/en/latest/admin_guide/debian_installation.html 

La configuración es muy muy básica para uso personal, por lo que no es recomendable abrirlo a Internet tal cual. Por supuesto, el usuario por defecto que trae se cambia.


## NFS Server

1. Instalamos los paquetes nfs-common nfs-kernel-server:

```
# apt install nfs-common nfs-kernel-server
```

2. Configuramos el fichero idmapd.conf tal y como se indica en la wiki del NFS http://wiki.linux-nfs.org/wiki/index.php/Nfsv4_configuration#Ubuntu 

La instalación del lado cliente depende del sistema operativo, que ya viene explicado en la Wiki anterior.

3. Compartimos la carpeta en nuestra red especificando lo siguiente en /etc/exports 


```
# Ruta Red1(Opciones) Red2(Opciones), donde Red1 y Red2 Pueden ser direcciones de red, dominios e IPs
# Más info en  $ man 5 exports

/home/{{ user }}/Videos 192.168.1.0/24(ro,all_squash,insecure,sync,no_subtree_check) vantergos(rw,sync,no_subtree_check) 

```
4. En el lado cliente, tenemos varias opciones dependiendo de la aplicación cliente que soporte nfs4. Para que nuestra carpeta se monte automáticamente en nuestro Host y que en caso de que estemos fuera y por tanto, no tengamos acceso a nuestra carpeta NFS, introducimos la siguiente entrada en /etc/fstab

```
# IP del server:ruta		Punto de montaje en nuestro Host	tipo	opciones

raspi:/home/{{ user }}/Videos	/home/{{ user }}/Videos/Multimedia	nfs	auto,nofail,rw	0	0
```


## Torrent server

1. Instalamos los siguientes paquetes:

```
# apt install transmission-cli transmission-daemon
```

2. Por defecto, cuando iniciamos transmission por init.d (por tanto, usando privilegios de root), el fichero de configuración se encuentra en /etc/transmission-daemon/settings.json. También, se puede correr manualmente la app con un usuario sin privilegios simplemente ejecutando `transmission-daemon`. En este caso, nos generará un fichero de configuración en ~/.config/transmission-daemon/settings.json

Después de modificar el fichero, hay que reiniciar el servicio y se actualizarán los cambios.

3. Podemos administrarlo desde nuestro Host simplemente introduciendo en el navegador la IP de la Raspberrypi y el puerto 9091.

Por seguridad, configurar bien la autenticación. Pendiente securizar, aunque sea para red local.

## Minecraft server

Realizado en Raspberry pi Model 4

1.- Instalamos primero el software necesario:

``` 
# apt install openjdk-8-jdk screen
```
2.- Creamos el usuario que va a arrancar el servidor y el directorio donde vamos a dejar los ficheros. En mi caso, he escogido `/srv`

```
# mkdir /srv/Mcserver
# useradd --system -M -d /srv/Mcserver/ -s /bin/rbash minecraft

```

Nota: si queremos hostear más instancias deberíamos crear subdirectorios que cuelguen de Mcserver.

3.- Antes de dar los permisos al usuario creado anteriormente, le asignaremos los permisos del usuario con el cual vamos a realizar la primera instalación y configuración del servidor.

```
# chown {{ user }}. /srv/Mcserver
```

Luego, descargamos la versión deseada de MC server. Yo he elegido PaperMC, a ver qué tal. La documentación aquí: https://paper.readthedocs.io/en/latest/index.html

```
$ cd {{ directorio_instalación }} && wget -O {{ nombre_archivo_jar }}.jar {{ url_serverpack }}
```

4.- Hacemos un primer arranque para comprobar que funciona bien. 

```
java -Xmx2G -Xms2G -jar {{ nombre_archivo_jar }}.jar
```
La primera ejecución dará error porque nos pide aceptar el EULA. Para ello, situados en la misma carpeta cambiamos el `eula=false` por `eula=true` con el siguiente comando

```
$ sed -i 's/false/true/' eula.txt
``` 

5.- Aún no podemos abrir el servidor como tal, ya que necesitamos que se ejecute en segundo plano, sin necesidad de una terminal conectada siempre. Para ello, crearemos los scripts de inicio. Antes de crear nada, paramos el servidor y cambiamos el propietaio y permisos de la carpeta.

```
# chown -R minecraft. /srv/Mcsever
# chmod -R 775 /srv/Mcserver
# gpasswd -a {{ user }} minecraft
```

**Nota importante:** A partir de ahora no se ha de ejecutar el servidor con otro usuario que no sea minecraft, ya que causaría problemas de permisos cuando se cargan nuevos chunks, puesto que éstos se guardarán en el servidor como ficheros creados por otro usuario.

Tenemos que añadir a nuestro usuario al grupo para que los permisos 775 tengan sentido, ya que necesitaremos de nuestro usuario para poder editar los ficheros de configuración. El usuario creado es de sistema y tendrá asignado una shell restrictiva que nos permita conectarnos a la consola del server. 

Hasta aquí le damos un reinicio a la Raspberry pi para que se apliquen los cambios al usuario.

6.- Ahora sí, optaremos por crearlo como scripts de systemd.

Fuente de los scripts de inicio https://minecraft.gamepedia.com/Tutorials/Server_startup_script

```
# vim /lib/systemd/system/mc-server.service

```

...con el siguiente contenido:

```
[Unit] 
Description=Instancia del servidor de Minecraft

[Service]
WorkingDirectory=/srv/Mcserver
User=minecraft
Group=minecraft
Type=forking
ExecStart=/usr/bin/screen -dmS minecraft /usr/bin/java -Xms2048M -Xmx2048M -jar paperMC-server_1.15.2.jar nogui

[Install]
WantedBy=multi-user.target

```

Finalmente, recargamos la configuración de daemos con:

```
# systemctl daemon-reload
```
...y lo habilitamos para que se inicie automáticamente:

```
# systemctl enable mc-server.service
```

Podemos iniciar el servidor con 

```
# systemctl start mc-server.service
```

Para conectarnos a la consola del servidor nos hacemos servir del user `minecraft`. Al no tener contraseña, nos logearemos al user desde root y ejecutamos el comando `$ screen -r`. Para salir del screen, pulsamos CTRL + A y luego D

### Mantenimiento

Automatizar las copias de seguridad es importante para poder restaurarlo en caso de corrupción, pérdida de datos o causas mayores. Nuestro script será ejecutado por cron y lo podremos dejar en cualquier ruta accesible para el usuario. En este caso, para la raspberrypi haremos que se ejecute desde el cron de un usuario normal.

1.- Creamos el siguiente script

[Ver fichero]

2.- Introducimos la entrada en el crontab

```
55 08 * * * {{ ruta_hacia_script }}/backup_mc-server.sh
```

Bonus: para evitar que se acumulen demasiados backups antiguos, podemos crear otra entrada de cron en nuestro servidor que elimine el más antiguo, dejando un límite de, por ejemplo 5 copias.

```
02 00 * * * /usr/bin/find /home/pi/Public/worlds_bk/* -mmin +7200  -exec rm "{}" \;
```
### Actualización a v1.17.x

1.- Creamos una nueva carpeta que alojará los ficheros de la nueva instancia. Nos descargamos el paquete correspondiente al servidor en la carpeta que hayamos creado. En este caso mantenemos PaperMC. 


2.- Antes de ejecutarlo, hemos de actualizar la versión 16 de java. Para ello seguiremos los pasos descritos aquí: https://adoptopenjdk.net/installation.html?variant=openjdk16&jvmVariant=hotspot#linux-pkg-deb

3.- Una vez instalado, seguiremos el procedimiento habitual descrito anteriormente desde el paso 4 del apartado de proyecto.

### Actualización a v1.18.x

1.- Creamos una nueva carpeta que alojará los ficheros de la nueva instancia. Nos descargamos el paquete correspondiente al servidor en la carpeta que hayamos creado. En este caso mantenemos PaperMC.

2.- Antes de ejecutarlo, hemos de actualizar la versión 17 de java. Nos descargamos el .deb con arquitectura arm32
```
$ wget https://download.bell-sw.com/java/17.0.2+9/bellsoft-jdk17.0.2+9-linux-arm32-vfp-hflt.deb
```
Y lo instalamos con 
```
# dpkg -i archivo .deb
```
3.- Una vez instalado, seguiremos el procedimiento habitual descrito anteriormente desde el paso 4 del apartado de proyecto.


## Configurar containers con LXC

**Se está pensando en migrar a la Raspberry pi Model 4** si se va a utilizar

Instalar lxc - La idea de usar LXC es para aislar el servidor web o cualquier servicio abierto a Internet de la raspi.

`Daemons asociados: lxc, lxcfs, lxc-net`

configuracion: https://www.engineyard.com/blog/isolation-linux-containers

Se crea usuaro nouid con pass ****** para usar lxc sin privilegios.

Configurar conectividad y contenedores sin privilegios: 
https://myles.sh/configuring-lxc-unprivileged-containers-in-debian-jessie/

Instalar seccomp para solucionar el problema despues de haber configurado el mapeo de uids-gids. 
Instalar raspberrypi-kernel-headers 

Problema: la raspberry pierde la conectividad (ver logs) al levantar un contenedor y asignarle la ip.
Solucion: añadir al fichero /etc/dhcpcd.conf la configuracion de interfaz lxcbr0
	interface lxcbr0
	static ip_address=10.0.30.1/24



Instalar ansible para raspberry pi via pip

Paquetes: python-pip libffi-dev libssl-dev cdbs sshpass

### Container
Acceso: desde usuario pi:
	ssh alpine-lxc


Provisionar web server
Usando ansible:
ansible-galaxy install robertdebock.mysql  robertdebock.php robertdebock.httpd



Instalar python


Iptables

Abrirlo a internet?


## Misceláneo
Sistema de monitorización?


GPIO pins

https://pinout.xyz/pinout
